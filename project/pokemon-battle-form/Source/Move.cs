﻿
using System.Collections;
using System;
/// <summary>
/// not sure if i'll be handling special effects
/// </summary>
[Serializable]
public class Move  {

    /// <summary>
    /// move name
    /// </summary>
    private string name;
    public string Name
    {
        get { return name; }
    }

    /// <summary>
    /// amount of maximum PP
    /// </summary>
    private int maxPP;
    public int MaxPP
    {
        get { return maxPP; }
    }

    /// <summary>
    /// amount of current PP
    /// </summary>
    private int currentPP;
    public int CurrentPP
    {
        get { return currentPP; }
    }

    /// <summary>
    /// attack power
    /// </summary>
    private int attack;
    public int Attack
    {
        get { return attack; }
    }

    /// <summary>
    /// What type is this Move? The type is used for
    /// calculating effectiveness against Pokemon.
    /// </summary>
    private SpecialType specialType;
    public SpecialType MoveType
    {
        get { return specialType; }
    }

    public Move(string a_name, int a_pp, int a_atk, SpecialType a_type)
    {
        name = a_name;
        maxPP = a_pp;
        currentPP = a_pp;
        attack = a_atk;
        specialType = a_type;
    }

    public Move()
    {

    }
    public virtual void Use()
    {
        if (currentPP <= 0)
            return;

        currentPP--;
    }

}
