﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace pokemon_battle_form
{
    public partial class PartyPokemonControl : UserControl
    {

        public Pokemon attachedPokemon = null;
        public PartyPokemonControl()
        {
            InitializeComponent();

            foreach (Control c in Controls)
            {
                BattleForm.ApplyFont(c);
            }
        }

        public void SetPokemon(Pokemon a_pokemon)
        {
            attachedPokemon = a_pokemon;
            NameLabel.Text = a_pokemon.Name.ToUpper();
            LevelLabel.Text = a_pokemon.Level.ToString();
            CurrHPLabel.Text = a_pokemon.CurrentHP.ToString() + "/";
            MaxHPLabel.Text = a_pokemon.MaxHP.ToString();
            HPBar.Maximum = a_pokemon.MaxHP;
            HPBar.Value = a_pokemon.CurrentHP;

        }

        private void button1_Click(object sender, EventArgs e)
        {

            (ParentForm as Party).RemovePokemon(attachedPokemon);
        }

        private void CurrHPLabel_Click(object sender, EventArgs e)
        {

        }

        private void MaxHPLabel_Click(object sender, EventArgs e)
        {

        }
    }
}
