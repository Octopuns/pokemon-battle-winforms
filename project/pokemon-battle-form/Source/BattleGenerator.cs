﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace pokemon_battle_form
{
    public static class BattleGenerator
    {

        private static Random randomiser;

        /// <summary>
        /// Random pokemon names
        /// </summary>
        private static string[] randomPokemonNames;
        /// <summary>
        /// Random move names
        /// </summary>
        private static string[] randomMoveNames;

        public static void Load()
        {
            randomiser = new Random(DateTime.Now.Second);


            randomPokemonNames = Properties.Resources.pokemon_names.Split('\n');


            randomMoveNames = Properties.Resources.move_names.Split('\n');
        }

        /// <summary>
        /// Create and return a complete random Pokemon
        /// </summary>
        /// <returns>The randomly generated pokemon</returns>
        public static Pokemon GetRandomPokemon()
        {
            int lvl = randomiser.Next() % 30;

            int hp = randomiser.Next() % 100 + 50;
            SpecialType st = (SpecialType)(randomiser.Next() % 5);
            string name = "DEFAULT";

            //get name from random names if they exist
            if (randomPokemonNames.Length != 0) {
                name = randomPokemonNames[randomiser.Next() % randomPokemonNames.Length];
                name = name.Replace("\r", "");
                    }

            Pokemon pm = new Pokemon(name, lvl, hp, st);

            //create random moves (up to four)
            int moveNum = (randomiser.Next() % 2 + 1) + 2;
            for (int i = 0; i < moveNum; i++)
                pm.AddMove(GetRandomMove());

            return pm;
        }

        /// <summary>
        /// Create and return a complete random move
        /// </summary>
        /// <returns></returns>
        public static Move GetRandomMove()
        {

            int pp = randomiser.Next() % 10 + 10;
            int atk = randomiser.Next() % 50 + 100;
            SpecialType st = (SpecialType)(randomiser.Next() % 5);
            //get name from random names if they exist
            string name = "DEFAULT";
            if (randomMoveNames.Length != 0)
                name = randomMoveNames[(int)(randomiser.Next() % randomMoveNames.Length)];
            Move move = new Move(name, pp, atk, st);
            return move;
        }


        /// <summary>
        /// Create and return a complete random move
        /// </summary>
        /// <returns></returns>
        public static Team GetRandomTeam()
        {

            Team team = new Team();

            //create pokemon
            int pokemonNum = (int)(randomiser.Next() % 5) + 1;
            for (int i = 0; i < pokemonNum; i++)
                team.AddPokemon(GetRandomPokemon());

            //TODO create inventory later

            return team;
        }


        #region IMAGELOADING

        /// <summary>
        /// Image Loading from Stack Overflow User: Icemanind - 
        /// http://stackoverflow.com/questions/27846337/select-and-download-random-image-from-google-c-sharp-visual-studio-2012
        /// </summary>
        private static readonly List<string> _topics = new List<string> { "dog", "cat", "bird", "tree", "lion", "giraffe", "frog", "fish", "tortoise", "canary", "penguin", "horse", "cow"};

        public static Image GetRandomImage()
        {
            string html = GetHtmlCode();
            //if no response, return
            if (string.IsNullOrEmpty(html))
                return null;

            List<string> urls = GetUrls(html);

            var rnd = new Random();

            int randomUrl = rnd.Next(0, urls.Count - 1);

            string luckyUrl = urls[randomUrl];

            byte[] image = GetImage(luckyUrl);

            if (image == null)
                return null;

            try
            {
                using (var ms = new MemoryStream(image))
                {
                    if(ms != null)
                        return Image.FromStream(ms);
                }
            }
            catch
            {
                return null;
            }

            return null;
        }

        private static string GetHtmlCode()
        {
            int topic = randomiser.Next(0, _topics.Count - 1);

            string url = "https://www.google.com/search?q=" + _topics[topic] + "&tbm = isch & as_q = &as_epq = &as_oq = &as_eq = &cr = &as_sitesearch = &safe = images & tbs = isz:s,ic: trans,iar: w";
            string data = "";

            try
            {
                var request = (HttpWebRequest)WebRequest.Create(url);
                request.Accept = "text/html, application/xhtml+xml, */*";
                request.UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64; Trident/7.0; rv:11.0) like Gecko";

                using (var response = request.GetResponse())
                {
                    using (var responseStream = response.GetResponseStream())
                    {
                        using (Stream dataStream = response.GetResponseStream())
                        {
                            if (dataStream == null)
                                return "";
                            using (var sr = new StreamReader(dataStream))
                            {
                                data = sr.ReadToEnd();
                            }
                        }
                        return data;
                    }
                }
            }
            catch (WebException ex)
            {
                if (ex.Status == WebExceptionStatus.ProtocolError &&
                    ex.Response != null)
                {
                    var resp = (HttpWebResponse)ex.Response;
                    if (resp.StatusCode == HttpStatusCode.NotFound)
                    {
                        // Do something
                    }
                    else
                    {
                        // Do something else
                    }
                }
                else
                {
                    // Do something else
                }

                return string.Empty;
            }

          
        }

        private static List<string> GetUrls(string html)
        {
            var urls = new List<string>();

            int ndx = html.IndexOf("\"ou\"", StringComparison.Ordinal);

            while (ndx >= 0)
            {
                ndx = html.IndexOf("\"", ndx + 4, StringComparison.Ordinal);
                ndx++;
                int ndx2 = html.IndexOf("\"", ndx, StringComparison.Ordinal);
                string url = html.Substring(ndx, ndx2 - ndx);
                urls.Add(url);
                ndx = html.IndexOf("\"ou\"", ndx2, StringComparison.Ordinal);
            }
            return urls;
        }

        private static byte[] GetImage(string url)
        {
            try
            {
                var request = (HttpWebRequest)WebRequest.Create(url);
                using (var response = request.GetResponse())
                {
                    using (var responseStream = response.GetResponseStream())
                    {
                        using (Stream dataStream = response.GetResponseStream())
                        {
                            if (dataStream == null)
                                return null;
                            using (var sr = new BinaryReader(dataStream))
                            {
                                byte[] bytes = sr.ReadBytes(100000000);

                                return bytes;
                            }
                        }
                    }
                }
            }
            catch (WebException ex)
            {
                if (ex.Status == WebExceptionStatus.ProtocolError &&
                    ex.Response != null)
                {
                    var resp = (HttpWebResponse)ex.Response;
                    if (resp.StatusCode == HttpStatusCode.NotFound)
                    {
                        // Do something
                    }
                    else
                    {
                        // Do something else
                    }
                }
                else
                {
                    // Do something else
                }

                return null;
            }


        }
        #endregion IMAGELOADING

    }

}
