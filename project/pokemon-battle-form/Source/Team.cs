﻿
using System.Collections.Generic;
using System.Xml.Serialization;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System;

[Serializable]
public class Team {

    /// <summary>
    /// array of pokemon
    /// </summary>
    
    public List<Pokemon> pokemon = new List<Pokemon>();

    /// <summary>
    /// Add a pokemon to this team
    /// </summary>
    /// <param name="a_pokemon"></param>
    public void AddPokemon(Pokemon a_pokemon)
    {
        pokemon.Add(a_pokemon);
    }

    public void RemovePokemon(Pokemon a_pokemon)
    {
        pokemon.Remove(a_pokemon);
    }

    /// <summary>
    /// Get the pokemon as an array
    /// </summary>
    /// <returns></returns>
    public Pokemon[] GetPokemon()
    {
        return pokemon.ToArray();
    }



}
