﻿using System.Collections.Generic;
using System.Windows.Forms;

namespace pokemon_battle_form
{
    public partial class Party : Form
    {
        private Team attachedTeam;
        public List<PartyPokemonControl> partyPokemonControls = new List<PartyPokemonControl>();

        public Party()
        {
            InitializeComponent();

       
        }

        /// <summary>
        /// Attach a team to this party window. Used to create panels.
        /// </summary>
        /// <param name="a_team"></param>
        public void SetTeam(Team a_team)
        {
            attachedTeam = a_team;
            RefreshParty();
        }

        /// <summary>
        /// Refresh party panels using attached team.
        /// </summary>
        public void RefreshParty()
        {
            flowLayoutPanel1.Controls.Clear();

            //generate new ones
            foreach(Pokemon p in attachedTeam.GetPokemon())
            {
                //create a party pokemon control
                PartyPokemonControl ppc = new PartyPokemonControl();
                ppc.SetPokemon(p);
                flowLayoutPanel1.Controls.Add(ppc);
                partyPokemonControls.Add(ppc);
            }
        }

        /// <summary>
        /// Remove a pokemon from the team - this calls the team.RemovePokemon()
        /// </summary>
        /// <param name="a_pokemon">Pokemon to remove</param>
        public void RemovePokemon(Pokemon a_pokemon)
        {
            if (attachedTeam.pokemon.Count > 1)
            {
                attachedTeam.RemovePokemon(a_pokemon);
                RefreshParty();
            }
        }
    }
}
