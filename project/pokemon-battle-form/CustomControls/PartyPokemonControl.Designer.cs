﻿namespace pokemon_battle_form
{
    partial class PartyPokemonControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.PokemonPartyPanel = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.MaxHPLabel = new System.Windows.Forms.Label();
            this.CurrHPLabel = new System.Windows.Forms.Label();
            this.HPBar = new System.Windows.Forms.ProgressBar();
            this.label4 = new System.Windows.Forms.Label();
            this.LevelLabel = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.NameLabel = new System.Windows.Forms.Label();
            this.PokemonPartyPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // PokemonPartyPanel
            // 
            this.PokemonPartyPanel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.PokemonPartyPanel.Controls.Add(this.button1);
            this.PokemonPartyPanel.Controls.Add(this.MaxHPLabel);
            this.PokemonPartyPanel.Controls.Add(this.CurrHPLabel);
            this.PokemonPartyPanel.Controls.Add(this.HPBar);
            this.PokemonPartyPanel.Controls.Add(this.label4);
            this.PokemonPartyPanel.Controls.Add(this.LevelLabel);
            this.PokemonPartyPanel.Controls.Add(this.label6);
            this.PokemonPartyPanel.Controls.Add(this.NameLabel);
            this.PokemonPartyPanel.Location = new System.Drawing.Point(3, 3);
            this.PokemonPartyPanel.Name = "PokemonPartyPanel";
            this.PokemonPartyPanel.Size = new System.Drawing.Size(408, 65);
            this.PokemonPartyPanel.TabIndex = 1;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Salmon;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button1.Location = new System.Drawing.Point(374, 7);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(20, 20);
            this.button1.TabIndex = 14;
            this.button1.Text = "X";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // MaxHPLabel
            // 
            this.MaxHPLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.MaxHPLabel.Location = new System.Drawing.Point(317, 26);
            this.MaxHPLabel.Name = "MaxHPLabel";
            this.MaxHPLabel.Size = new System.Drawing.Size(64, 31);
            this.MaxHPLabel.TabIndex = 13;
            this.MaxHPLabel.Text = "25";
            this.MaxHPLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.MaxHPLabel.UseCompatibleTextRendering = true;
            this.MaxHPLabel.Click += new System.EventHandler(this.MaxHPLabel_Click);
            // 
            // CurrHPLabel
            // 
            this.CurrHPLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.CurrHPLabel.Location = new System.Drawing.Point(227, 26);
            this.CurrHPLabel.Name = "CurrHPLabel";
            this.CurrHPLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.CurrHPLabel.Size = new System.Drawing.Size(84, 31);
            this.CurrHPLabel.TabIndex = 12;
            this.CurrHPLabel.Text = "25/";
            this.CurrHPLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.CurrHPLabel.UseCompatibleTextRendering = true;
            this.CurrHPLabel.Click += new System.EventHandler(this.CurrHPLabel_Click);
            // 
            // HPBar
            // 
            this.HPBar.Location = new System.Drawing.Point(56, 34);
            this.HPBar.Name = "HPBar";
            this.HPBar.Size = new System.Drawing.Size(165, 11);
            this.HPBar.TabIndex = 11;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(3, 34);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(29, 20);
            this.label4.TabIndex = 10;
            this.label4.Text = "HP:";
            this.label4.UseCompatibleTextRendering = true;
            // 
            // LevelLabel
            // 
            this.LevelLabel.AutoSize = true;
            this.LevelLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.LevelLabel.Location = new System.Drawing.Point(317, 7);
            this.LevelLabel.Name = "LevelLabel";
            this.LevelLabel.Size = new System.Drawing.Size(24, 24);
            this.LevelLabel.TabIndex = 9;
            this.LevelLabel.Text = "25";
            this.LevelLabel.UseCompatibleTextRendering = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label6.Location = new System.Drawing.Point(280, 8);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(16, 20);
            this.label6.TabIndex = 8;
            this.label6.Text = ":L";
            this.label6.UseCompatibleTextRendering = true;
            // 
            // NameLabel
            // 
            this.NameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.NameLabel.Location = new System.Drawing.Point(3, 5);
            this.NameLabel.Name = "NameLabel";
            this.NameLabel.Size = new System.Drawing.Size(261, 31);
            this.NameLabel.TabIndex = 7;
            this.NameLabel.Text = "GEODUDE";
            this.NameLabel.UseCompatibleTextRendering = true;
            // 
            // PartyPokemonControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.PokemonPartyPanel);
            this.Name = "PartyPokemonControl";
            this.Size = new System.Drawing.Size(415, 68);
            this.PokemonPartyPanel.ResumeLayout(false);
            this.PokemonPartyPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel PokemonPartyPanel;
        private System.Windows.Forms.Label MaxHPLabel;
        private System.Windows.Forms.Label CurrHPLabel;
        private System.Windows.Forms.ProgressBar HPBar;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label LevelLabel;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label NameLabel;
        private System.Windows.Forms.Button button1;
    }
}
