﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.Drawing.Text;
using System.Runtime.InteropServices;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace pokemon_battle_form
{
    public partial class BattleForm : Form
    {
        Team playerTeam;

        /// <summary>
        /// Player Pokemon
        /// </summary>
        Pokemon pokemon1;

        /// <summary>
        /// Opponent Pokemon
        /// </summary>
        Pokemon pokemon2;

        Party partyForm = null;
        Button[] battleButtons = new Button[4];

        //Create your private font collection object.
        static PrivateFontCollection pfc = new PrivateFontCollection();

        //create states
        public enum PlayerState
        {
            SELECTION,
            PARTY,
            CAUGHT,
            MOVES
        };

        public PlayerState playerState;

        public BattleForm()
        {
        
            InitializeComponent();
            InitialiseCustomFonts();

          
            ApplyFont(this);
          
            BattleGenerator.Load();
            RandomlyGeneratePokemon();

            playerTeam = new Team();
            playerTeam.AddPokemon( pokemon1 );

            partyForm = new Party();

            // Set the Parent Form of the Child window.
            partyForm.MdiParent = this.MdiParent;
            partyForm.SetTeam(playerTeam);

            //hook up battle buttons
            battleButtons[0] = BattleButton1;
            battleButtons[1] = BattleButton2;
            battleButtons[2] = BattleButton3;
            battleButtons[3] = BattleButton4;
       
            BeginBattle();
        }

        /// <summary>
        /// Start the battle.
        /// </summary>
        private void BeginBattle()
        {
            UpdatePokemonPanelDetails(pokemon1, true);
            UpdatePokemonPanelDetails(pokemon2, false);
 
            StartState(PlayerState.SELECTION);
        }

        /// <summary>
        /// Save Data using binary serialization
        /// </summary>
        /// <param name="filename"></param>
        public void SaveData(string filename)
        {
            Stream fileStream = new FileStream(filename, FileMode.Create, FileAccess.Write, FileShare.None);
            BinaryFormatter bformatter = new BinaryFormatter();

            bformatter.Serialize(fileStream, playerTeam);
            fileStream.Close();
        }

        /// <summary>
        /// Load data using Binary Deserialization
        /// </summary>
        /// <param name="filename"></param>
        public void LoadData(string filename)
        {
            Stream fileStream = new FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.Read);

            BinaryFormatter bformatter = new BinaryFormatter();
            playerTeam = bformatter.Deserialize(fileStream) as Team;
            fileStream.Close();


            if (playerTeam.pokemon.Count == 0)
                pokemon1 = BattleGenerator.GetRandomPokemon();

            pokemon1 = playerTeam.GetPokemon()[0];
            partyForm.SetTeam(playerTeam);
            UpdatePokemonPanelDetails(pokemon1, true);
        }

        /// <summary>
        /// Switch between player states, update buttons text and bind/rebind button
        /// events based on current state
        /// </summary>
        /// <param name="eState"></param>
        private void StartState(PlayerState eState)
        {

            //stop previous state
            switch(playerState)
            {
                case PlayerState.SELECTION:
                    //unbind
                    BattleButton1.MouseClick -= OpenMovesEvent;
                    BattleButton3.MouseClick -= OpenPartyEvent;
                    BattleButton2.MouseClick -= CatchPokemonEvent;
                    BattleButton4.MouseClick -= EndBattleEvent;
                    break;
                case PlayerState.PARTY:
                    BattleButton3.MouseClick -= ClosePartyEvent;           
                    break;
                case PlayerState.MOVES:
                    //unbind buttons maybe
                    break;
            }

      
            //start state
            switch (eState)
            {
                case PlayerState.SELECTION:
                    EnableButtons();
                    BattleProgressLabel.Text = "What will " + pokemon1.Name.ToUpper() + " do?";

                    BattleButton1.Text = "FIGHT";
                    BattleButton1.MouseClick += OpenMovesEvent;

                    BattleButton2.Text = "CATCH";
                    BattleButton2.MouseClick += CatchPokemonEvent;

                    BattleButton3.Text = "POKeMON";
                    BattleButton3.MouseClick += OpenPartyEvent;

                    BattleButton4.Text = "RUN";
                    BattleButton4.MouseClick += EndBattleEvent;
                    break;

                case PlayerState.PARTY:
                    BattleProgressLabel.Text = "Will you switch Pokemon?";
                    DisableButtons();
                    BattleButton3.Enabled = true;
                    BattleButton3.Text = "CANCEL";
                    BattleButton3.MouseClick -= OpenPartyEvent;
                    BattleButton3.MouseClick += ClosePartyEvent;
                    break;

                case PlayerState.MOVES:
                    for(int i = 0; i < battleButtons.Length; i++)
                    {
                        battleButtons[i].Text = "";
                        battleButtons[i].MouseClick -= CloseMovesEvent;
                        battleButtons[i].Enabled = false;
                    }

                    for(int i = 0; i < pokemon1.moves.Count; i++)
                    {
                        battleButtons[i].Enabled = true;
                        battleButtons[i].MouseClick += CloseMovesEvent;
                        battleButtons[i].Text = pokemon1.moves[i].Name;
                    }

                    //bind buttons
                    BattleProgressLabel.Text = "Choose a move.";
                    break;
                case PlayerState.CAUGHT:
                    DisableButtons();
                    BattleButton3.Enabled = true;
                    BattleButton3.Text = "CANCEL";
                    BattleButton3.MouseClick += ReturnToSelectionEvent;
                    BattleButton2.MouseClick -= CatchPokemonEvent;
                    break;
            }

            playerState = eState;
        }

        /// <summary>
        /// Enable all four choice buttons
        /// </summary>
        private void EnableButtons()
        {
            BattleButton1.Enabled = true;
            BattleButton2.Enabled = true;
            BattleButton3.Enabled = true;
            BattleButton4.Enabled = true;
        }

        /// <summary>
        /// Disable all four choice buttons
        /// </summary>
        private void DisableButtons()
        {
            BattleButton1.Enabled = false;
            BattleButton2.Enabled = false;
            BattleButton3.Enabled = false;
            BattleButton4.Enabled = false;
        }

        /// <summary>
        /// Setup custom font
        /// </summary>
        private void InitialiseCustomFonts()
        {
            

            //Select your font from the resources.
            //My font here is "Digireu.ttf"
            int fontLength = Properties.Resources.Pokemon_GB.Length;

            // create a buffer to read in to
            byte[] fontdata = Properties.Resources.Pokemon_GB;

            // create an unsafe memory block for the font data
            System.IntPtr data = Marshal.AllocCoTaskMem(fontLength);

            // copy the bytes to the unsafe memory block
            Marshal.Copy(fontdata, 0, data, fontLength);

            // pass the font to the font collection
            pfc.AddMemoryFont(data, fontLength);

            // free up the unsafe memory
            Marshal.FreeCoTaskMem(data);

  
          
        }

        /// <summary>
        /// Recursive function to apply font to all controls
        /// </summary>
        /// <param name="c"></param>
        public static void ApplyFont(Control c )
        {

            c.Font = new Font(pfc.Families[0], c.Font.Size);
     
            foreach (Control c2 in c.Controls)
            {
                ApplyFont(c2);
            }

        }

        /// <summary>
        /// Randomly generate two new pokemon
        /// </summary>
        private void RandomlyGeneratePokemon()
        {
            pokemon1 = BattleGenerator.GetRandomPokemon();
            pokemon2 = BattleGenerator.GetRandomPokemon();

            Random r = new Random();

            pokemon1.AlterHealth(-r.Next() % 100 );
            pokemon2.AlterHealth(-r.Next() % 100 );
        }

        /// <summary>
        /// Update a pokemon panel with the details of that pokemon
        /// </summary>
        /// <param name="a_pokemon">Pokemon to take details from</param>
        /// <param name="player">Is the pokemon a player pokemon?</param>
        private void UpdatePokemonPanelDetails(Pokemon a_pokemon, bool player)
        {
            if (player)
            {
                P1NameLabel.Text = a_pokemon.Name.ToUpper();
                P1LevelLabel.Text = a_pokemon.Level.ToString();
                P1CurrHPLabel.Text = a_pokemon.CurrentHP.ToString() + "/";
                P1MaxHPLabel.Text = a_pokemon.MaxHP.ToString();
                P1HPBar.Maximum = a_pokemon.MaxHP;
                P1HPBar.Value = a_pokemon.CurrentHP;
      
            }
            else
            {
                P2NameLabel.Text = a_pokemon.Name.ToUpper();
                P2LevelLabel.Text = a_pokemon.Level.ToString();
                P2CurrHPLabel.Text = a_pokemon.CurrentHP.ToString() + "/";
                P2MaxHPLabel.Text = a_pokemon.MaxHP.ToString();
                P2HPBar.Maximum = a_pokemon.MaxHP;
                P2HPBar.Value = a_pokemon.CurrentHP;    
            }
       
        }

        /// <summary>
        /// Handle keypresses - mainly for exit
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BattleForm_KeyPress(object sender, KeyPressEventArgs e)
        {
            //implement an escape function
            if (e.KeyChar == (char)Keys.Escape)
            {
                Application.Exit();
            }

        }

        #region BUTTONEVENTS
        private void ReturnToSelectionEvent(object sender, MouseEventArgs e)
        {
            StartState(PlayerState.SELECTION);
        }

        private void OpenPartyEvent(object sender, EventArgs e)
        {
            // Display the new form.
            partyForm = new Party();
           
            partyForm.SetTeam(playerTeam);
            partyForm.Show();
            ApplyFont(partyForm);
            StartState(PlayerState.PARTY);
        }

        private void ClosePartyEvent(object sender, EventArgs e)
        {
            // Display the new form.
            partyForm.Close();
            StartState(PlayerState.SELECTION);
            pokemon1 = playerTeam.GetPokemon()[0];
        }

        private void OpenMovesEvent(object sender, EventArgs e)
        {    
            StartState(PlayerState.MOVES);
        }

        private void CloseMovesEvent(object sender, EventArgs e)
        {
            StartState(PlayerState.SELECTION);
        }

        private void EndBattleEvent(object sender, EventArgs e)
        {
            ParentForm.Close();
        }

        private void CatchPokemonEvent(object sender, MouseEventArgs e)
        {
            if (playerTeam.pokemon.Count < 6)
            {
                playerTeam.AddPokemon(pokemon2);
                BattleProgressLabel.Text = "You caught " + pokemon2.Name.ToString() + "!";
                pokemon2 = BattleGenerator.GetRandomPokemon();
                P2PokemonImage_MouseClick(null, null);
                UpdatePokemonPanelDetails(pokemon2, false);
               
                StartState(PlayerState.CAUGHT);
            }
            else
            {
                BattleProgressLabel.Text = "You already have 6 Pokemon. Don't be greedy.";
            }
        }
        #endregion BUTTONEVENTS

        /// <summary>
        /// Load random image on mouse click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void P2PokemonImage_MouseClick(object sender, MouseEventArgs e)
        {
            P2PokemonImage.BackColor = BackColor;
            P2PokemonImage.Image = BattleGenerator.GetRandomImage();
        }

        /// <summary>
        /// Load random image on mouse click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void P1PokemonImage_MouseClick(object sender, MouseEventArgs e)
        {
            P1PokemonImage.BackColor = BackColor;
            P1PokemonImage.Image = BattleGenerator.GetRandomImage();
            if (P1PokemonImage.Image != null)
            {
                Bitmap copy = new Bitmap(P1PokemonImage.Image);
                copy.Save(System.Environment.CurrentDirectory + "/" + pokemon1.Name.ToString() + ".png");
            }
        }


    }
}
