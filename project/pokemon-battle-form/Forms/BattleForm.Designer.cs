﻿namespace pokemon_battle_form
{
    partial class BattleForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.P2Panel = new System.Windows.Forms.Panel();
            this.P2MaxHPLabel = new System.Windows.Forms.Label();
            this.P2CurrHPLabel = new System.Windows.Forms.Label();
            this.P2HPBar = new System.Windows.Forms.ProgressBar();
            this.label4 = new System.Windows.Forms.Label();
            this.P2LevelLabel = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.P2NameLabel = new System.Windows.Forms.Label();
            this.P1PokemonImage = new System.Windows.Forms.PictureBox();
            this.P2PokemonImage = new System.Windows.Forms.PictureBox();
            this.P1Panel = new System.Windows.Forms.Panel();
            this.P1MaxHPLabel = new System.Windows.Forms.Label();
            this.P1CurrHPLabel = new System.Windows.Forms.Label();
            this.P1HPBar = new System.Windows.Forms.ProgressBar();
            this.HPLabel = new System.Windows.Forms.Label();
            this.P1LevelLabel = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.P1NameLabel = new System.Windows.Forms.Label();
            this.BattleButton1 = new System.Windows.Forms.Button();
            this.BattleButton2 = new System.Windows.Forms.Button();
            this.BattleButton3 = new System.Windows.Forms.Button();
            this.BattleButton4 = new System.Windows.Forms.Button();
            this.BattleProgressLabel = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.P2Panel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.P1PokemonImage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.P2PokemonImage)).BeginInit();
            this.P1Panel.SuspendLayout();
            this.SuspendLayout();
            // 
            // P2Panel
            // 
            this.P2Panel.Controls.Add(this.P2MaxHPLabel);
            this.P2Panel.Controls.Add(this.P2CurrHPLabel);
            this.P2Panel.Controls.Add(this.P2HPBar);
            this.P2Panel.Controls.Add(this.label4);
            this.P2Panel.Controls.Add(this.P2LevelLabel);
            this.P2Panel.Controls.Add(this.label6);
            this.P2Panel.Controls.Add(this.P2NameLabel);
            this.P2Panel.Location = new System.Drawing.Point(27, 12);
            this.P2Panel.Name = "P2Panel";
            this.P2Panel.Size = new System.Drawing.Size(329, 122);
            this.P2Panel.TabIndex = 12;
            // 
            // P2MaxHPLabel
            // 
            this.P2MaxHPLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.P2MaxHPLabel.Location = new System.Drawing.Point(130, 82);
            this.P2MaxHPLabel.Name = "P2MaxHPLabel";
            this.P2MaxHPLabel.Size = new System.Drawing.Size(92, 40);
            this.P2MaxHPLabel.TabIndex = 6;
            this.P2MaxHPLabel.Text = "25";
            this.P2MaxHPLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.P2MaxHPLabel.UseCompatibleTextRendering = true;
            // 
            // P2CurrHPLabel
            // 
            this.P2CurrHPLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.P2CurrHPLabel.Location = new System.Drawing.Point(31, 82);
            this.P2CurrHPLabel.Name = "P2CurrHPLabel";
            this.P2CurrHPLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.P2CurrHPLabel.Size = new System.Drawing.Size(81, 40);
            this.P2CurrHPLabel.TabIndex = 5;
            this.P2CurrHPLabel.Text = "25/";
            this.P2CurrHPLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.P2CurrHPLabel.UseCompatibleTextRendering = true;
            // 
            // P2HPBar
            // 
            this.P2HPBar.Location = new System.Drawing.Point(53, 68);
            this.P2HPBar.Name = "P2HPBar";
            this.P2HPBar.Size = new System.Drawing.Size(138, 11);
            this.P2HPBar.TabIndex = 4;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(9, 68);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(29, 20);
            this.label4.TabIndex = 3;
            this.label4.Text = "HP:";
            this.label4.UseCompatibleTextRendering = true;
            // 
            // P2LevelLabel
            // 
            this.P2LevelLabel.AutoSize = true;
            this.P2LevelLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.P2LevelLabel.Location = new System.Drawing.Point(291, 29);
            this.P2LevelLabel.Name = "P2LevelLabel";
            this.P2LevelLabel.Size = new System.Drawing.Size(24, 24);
            this.P2LevelLabel.TabIndex = 2;
            this.P2LevelLabel.Text = "25";
            this.P2LevelLabel.UseCompatibleTextRendering = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(265, 29);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(20, 24);
            this.label6.TabIndex = 1;
            this.label6.Text = ":L";
            this.label6.UseCompatibleTextRendering = true;
            // 
            // P2NameLabel
            // 
            this.P2NameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F);
            this.P2NameLabel.Location = new System.Drawing.Point(9, 29);
            this.P2NameLabel.Name = "P2NameLabel";
            this.P2NameLabel.Size = new System.Drawing.Size(249, 27);
            this.P2NameLabel.TabIndex = 0;
            this.P2NameLabel.Text = "GEODUDE";
            this.P2NameLabel.UseCompatibleTextRendering = true;
            // 
            // P1PokemonImage
            // 
            this.P1PokemonImage.BackColor = System.Drawing.SystemColors.ControlDark;
            this.P1PokemonImage.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.P1PokemonImage.Location = new System.Drawing.Point(27, 152);
            this.P1PokemonImage.Name = "P1PokemonImage";
            this.P1PokemonImage.Size = new System.Drawing.Size(299, 191);
            this.P1PokemonImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.P1PokemonImage.TabIndex = 11;
            this.P1PokemonImage.TabStop = false;
            this.P1PokemonImage.MouseClick += new System.Windows.Forms.MouseEventHandler(this.P1PokemonImage_MouseClick);
            // 
            // P2PokemonImage
            // 
            this.P2PokemonImage.BackColor = System.Drawing.SystemColors.ControlDark;
            this.P2PokemonImage.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.P2PokemonImage.Location = new System.Drawing.Point(383, 12);
            this.P2PokemonImage.Name = "P2PokemonImage";
            this.P2PokemonImage.Size = new System.Drawing.Size(308, 187);
            this.P2PokemonImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.P2PokemonImage.TabIndex = 10;
            this.P2PokemonImage.TabStop = false;
            this.P2PokemonImage.MouseClick += new System.Windows.Forms.MouseEventHandler(this.P2PokemonImage_MouseClick);
            // 
            // P1Panel
            // 
            this.P1Panel.Controls.Add(this.P1MaxHPLabel);
            this.P1Panel.Controls.Add(this.P1CurrHPLabel);
            this.P1Panel.Controls.Add(this.P1HPBar);
            this.P1Panel.Controls.Add(this.HPLabel);
            this.P1Panel.Controls.Add(this.P1LevelLabel);
            this.P1Panel.Controls.Add(this.label1);
            this.P1Panel.Controls.Add(this.P1NameLabel);
            this.P1Panel.Location = new System.Drawing.Point(383, 222);
            this.P1Panel.Name = "P1Panel";
            this.P1Panel.Size = new System.Drawing.Size(308, 116);
            this.P1Panel.TabIndex = 9;
            // 
            // P1MaxHPLabel
            // 
            this.P1MaxHPLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.P1MaxHPLabel.Location = new System.Drawing.Point(139, 82);
            this.P1MaxHPLabel.Name = "P1MaxHPLabel";
            this.P1MaxHPLabel.Size = new System.Drawing.Size(145, 39);
            this.P1MaxHPLabel.TabIndex = 6;
            this.P1MaxHPLabel.Text = "25";
            this.P1MaxHPLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.P1MaxHPLabel.UseCompatibleTextRendering = true;
            // 
            // P1CurrHPLabel
            // 
            this.P1CurrHPLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.P1CurrHPLabel.Location = new System.Drawing.Point(9, 82);
            this.P1CurrHPLabel.Name = "P1CurrHPLabel";
            this.P1CurrHPLabel.Size = new System.Drawing.Size(110, 39);
            this.P1CurrHPLabel.TabIndex = 5;
            this.P1CurrHPLabel.Text = "25/";
            this.P1CurrHPLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.P1CurrHPLabel.UseCompatibleTextRendering = true;
            // 
            // P1HPBar
            // 
            this.P1HPBar.Location = new System.Drawing.Point(62, 68);
            this.P1HPBar.Name = "P1HPBar";
            this.P1HPBar.Size = new System.Drawing.Size(139, 11);
            this.P1HPBar.TabIndex = 4;
            // 
            // HPLabel
            // 
            this.HPLabel.AutoSize = true;
            this.HPLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HPLabel.Location = new System.Drawing.Point(9, 68);
            this.HPLabel.Name = "HPLabel";
            this.HPLabel.Size = new System.Drawing.Size(29, 20);
            this.HPLabel.TabIndex = 3;
            this.HPLabel.Text = "HP:";
            this.HPLabel.UseCompatibleTextRendering = true;
            // 
            // P1LevelLabel
            // 
            this.P1LevelLabel.AutoSize = true;
            this.P1LevelLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.P1LevelLabel.Location = new System.Drawing.Point(271, 28);
            this.P1LevelLabel.Name = "P1LevelLabel";
            this.P1LevelLabel.Size = new System.Drawing.Size(24, 24);
            this.P1LevelLabel.TabIndex = 2;
            this.P1LevelLabel.Text = "25";
            this.P1LevelLabel.UseCompatibleTextRendering = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(245, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(20, 24);
            this.label1.TabIndex = 1;
            this.label1.Text = ":L";
            this.label1.UseCompatibleTextRendering = true;
            // 
            // P1NameLabel
            // 
            this.P1NameLabel.AutoSize = true;
            this.P1NameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F);
            this.P1NameLabel.Location = new System.Drawing.Point(13, 28);
            this.P1NameLabel.Name = "P1NameLabel";
            this.P1NameLabel.Size = new System.Drawing.Size(120, 25);
            this.P1NameLabel.TabIndex = 0;
            this.P1NameLabel.Text = "WEEPINBELL";
            this.P1NameLabel.UseCompatibleTextRendering = true;
            // 
            // BattleButton1
            // 
            this.BattleButton1.AutoSize = true;
            this.BattleButton1.BackColor = System.Drawing.Color.Transparent;
            this.BattleButton1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.75F);
            this.BattleButton1.Location = new System.Drawing.Point(332, 371);
            this.BattleButton1.Name = "BattleButton1";
            this.BattleButton1.Size = new System.Drawing.Size(170, 44);
            this.BattleButton1.TabIndex = 1;
            this.BattleButton1.Text = "FIGHT";
            this.BattleButton1.UseCompatibleTextRendering = true;
            this.BattleButton1.UseVisualStyleBackColor = false;
            // 
            // BattleButton2
            // 
            this.BattleButton2.BackColor = System.Drawing.Color.Transparent;
            this.BattleButton2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.75F);
            this.BattleButton2.Location = new System.Drawing.Point(508, 371);
            this.BattleButton2.Name = "BattleButton2";
            this.BattleButton2.Size = new System.Drawing.Size(170, 41);
            this.BattleButton2.TabIndex = 0;
            this.BattleButton2.Text = "CATCH";
            this.BattleButton2.UseCompatibleTextRendering = true;
            this.BattleButton2.UseVisualStyleBackColor = false;
            // 
            // BattleButton3
            // 
            this.BattleButton3.BackColor = System.Drawing.Color.Transparent;
            this.BattleButton3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.75F);
            this.BattleButton3.Location = new System.Drawing.Point(332, 418);
            this.BattleButton3.Name = "BattleButton3";
            this.BattleButton3.Size = new System.Drawing.Size(170, 41);
            this.BattleButton3.TabIndex = 3;
            this.BattleButton3.Text = "POKeMON";
            this.BattleButton3.UseCompatibleTextRendering = true;
            this.BattleButton3.UseVisualStyleBackColor = false;
            // 
            // BattleButton4
            // 
            this.BattleButton4.BackColor = System.Drawing.Color.Transparent;
            this.BattleButton4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.75F);
            this.BattleButton4.Location = new System.Drawing.Point(508, 418);
            this.BattleButton4.Name = "BattleButton4";
            this.BattleButton4.Size = new System.Drawing.Size(170, 41);
            this.BattleButton4.TabIndex = 2;
            this.BattleButton4.Text = "SAVE";
            this.BattleButton4.UseCompatibleTextRendering = true;
            this.BattleButton4.UseVisualStyleBackColor = false;
            // 
            // BattleProgressLabel
            // 
            this.BattleProgressLabel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BattleProgressLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.BattleProgressLabel.Location = new System.Drawing.Point(27, 370);
            this.BattleProgressLabel.Name = "BattleProgressLabel";
            this.BattleProgressLabel.Size = new System.Drawing.Size(283, 101);
            this.BattleProgressLabel.TabIndex = 0;
            this.BattleProgressLabel.Text = "What will WEEPINBELL do?";
            this.BattleProgressLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.BattleProgressLabel.UseCompatibleTextRendering = true;
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label3.Location = new System.Drawing.Point(400, 202);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(278, 17);
            this.label3.TabIndex = 14;
            this.label3.Text = "click above to download random image";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label2.Location = new System.Drawing.Point(33, 346);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(278, 17);
            this.label2.TabIndex = 15;
            this.label2.Text = "click above to download random image";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // BattleForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(703, 475);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.BattleButton1);
            this.Controls.Add(this.BattleButton2);
            this.Controls.Add(this.P2Panel);
            this.Controls.Add(this.BattleButton3);
            this.Controls.Add(this.BattleProgressLabel);
            this.Controls.Add(this.BattleButton4);
            this.Controls.Add(this.P1PokemonImage);
            this.Controls.Add(this.P2PokemonImage);
            this.Controls.Add(this.P1Panel);
            this.KeyPreview = true;
            this.Name = "BattleForm";
            this.Text = "BattleForm";
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.BattleForm_KeyPress);
            this.P2Panel.ResumeLayout(false);
            this.P2Panel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.P1PokemonImage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.P2PokemonImage)).EndInit();
            this.P1Panel.ResumeLayout(false);
            this.P1Panel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel P2Panel;
        private System.Windows.Forms.Label P2MaxHPLabel;
        private System.Windows.Forms.Label P2CurrHPLabel;
        private System.Windows.Forms.ProgressBar P2HPBar;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label P2LevelLabel;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label P2NameLabel;
        private System.Windows.Forms.PictureBox P1PokemonImage;
        private System.Windows.Forms.PictureBox P2PokemonImage;
        private System.Windows.Forms.Panel P1Panel;
        private System.Windows.Forms.Label P1MaxHPLabel;
        private System.Windows.Forms.Label P1CurrHPLabel;
        private System.Windows.Forms.ProgressBar P1HPBar;
        private System.Windows.Forms.Label HPLabel;
        private System.Windows.Forms.Label P1LevelLabel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label P1NameLabel;
        private System.Windows.Forms.Button BattleButton1;
        private System.Windows.Forms.Button BattleButton2;
        private System.Windows.Forms.Button BattleButton3;
        private System.Windows.Forms.Button BattleButton4;
        private System.Windows.Forms.Label BattleProgressLabel;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
    }
}