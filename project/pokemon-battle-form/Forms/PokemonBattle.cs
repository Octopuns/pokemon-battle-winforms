﻿using System;
using System.IO;
using System.Windows.Forms;

namespace pokemon_battle_form
{
    public partial class PokemonBattle : Form
    {
        BattleForm battleForm;

        public PokemonBattle()
        {
            InitializeComponent();
          
        }

        /// <summary>
        /// Detect KeyPress's for Form1
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Form1_KeyPress(object sender, KeyPressEventArgs e)
        {
            //implement an escape function
            if(e.KeyChar == (char)Keys.Escape)
            {
                Application.Exit();
            }
        }

        /// <summary>
        /// Close previous battleform and open new one
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if(battleForm != null)
                battleForm.Close();

            battleForm = new BattleForm();
            // Set the Parent Form of the Child window.
            battleForm.MdiParent = this;
            // Display the new form.
            battleForm.Show();
        }

        private void closeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        /// <summary>
        /// Handle load event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void loadMenuItem_Click(object sender, EventArgs e)
        {
            if (battleForm == null)
                return;

            openFileDialog1.InitialDirectory = System.Environment.CurrentDirectory;
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                if (File.Exists(openFileDialog1.FileName))
                {
                    battleForm.LoadData(openFileDialog1.FileName);
                }
            }

        }

        /// <summary>
        /// Handle save event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            saveFileDialog1.InitialDirectory = System.Environment.CurrentDirectory;
            if ( saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                battleForm.SaveData(saveFileDialog1.FileName);
            }
        }

    }
}
