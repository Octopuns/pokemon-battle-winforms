﻿
using System.Collections.Generic;
using System;

public enum SpecialType
{
    FIRE,
    WATER,
    GRASS,
    LIGHT,
    DARK
};

/// <summary>
/// not yet handling speed, defense, etc
/// </summary>
[Serializable]
public class Pokemon
{

    /// <summary>
    /// move name
    /// </summary>
    private string name;

    public string Name
    {
        get { return name; }
    }

    /// <summary>
    /// level of the pokemon
    /// </summary>
    private int level;
    public int Level
    {
        get { return level; }
    }

    /// <summary>
    /// amount of maximum HP
    /// </summary>
    private int maxHP;
    public int MaxHP
    {
        get { return maxHP; }
    }

    /// <summary>
    /// amount of current HP
    /// </summary>
    private int currentHP;
    public int CurrentHP
    { 
        get { return currentHP; }
    }

    /// <summary>
    /// Has the Pokemon fainted?
    /// </summary>
    public bool IsFainted
    {
        get { return currentHP <= 0; }
    }

    /// <summary>
    /// What type is this Pokemon? The type is used for
    /// calculating effectiveness of moves.
    /// </summary>
    private SpecialType specialType;
    public SpecialType PokemonType
    {
        get { return specialType; }
    }


    public List<Move> moves = new List<Move>();

    public void AddMove(Move a_move)
    {
        moves.Add(a_move);
    }

    public void AlterHealth(int a_change)
    {
        currentHP += a_change;

        if (currentHP < 0)
            currentHP = 0;
    }
    /// <summary>
    /// Construct the Pokemon
    /// </summary>
    /// <param name="a_name"></param>
    /// <param name="a_level"></param>
    /// <param name="a_maxHP"></param>
    /// <param name="a_type"></param>
    public Pokemon(string a_name, int a_level, int a_maxHP, SpecialType a_type)
    {
        name = a_name;
        maxHP = a_maxHP;
        currentHP = maxHP;
        level = a_level;
        specialType = a_type;
    }

    public Pokemon()
    {

    }
}
